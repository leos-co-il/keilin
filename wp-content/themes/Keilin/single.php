<?php

the_post();
get_header();
$fields = get_fields();

?>

<article class="scheme-post-back p-block">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center align-items-start">
			<div class="col-12 justify-content-start hide-title-post">
				<h1 class="block-title"><?php the_title(); ?></h1>
			</div>
			<div class="col-lg col-12 d-flex flex-column align-items-start justify-content-start post-content-col">
				<h1 class="block-title hide-normal-title"><?php the_title(); ?></h1>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['post_files']) : ?>
					<div class="row justify-content-start align-items-stretch">
						<div class="col-12">
							<h4 class="files-title"><?= $fields['files_title']; ?></h4>
						</div>
						<?php foreach ($fields['post_files'] as $x => $file) : ?>
							<div class="col-auto">
								<a href="<?= $file; ?>" class="file-post">
									<?= svg_simple(ICONS.'file.svg'); ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-1 hide-this"></div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-xl-4 col-lg-5 col-12 graph-about-col post-img-main">
					<div class="about-video" <?php if(has_post_thumbnail()): ?>
						style="background-image: url('<?= postThumb(); ?>')"<?php endif; ?>>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($fields['single_video_slider']) : ?>
			<div class="row my-4">
				<div class="col-12 arrows-slider process-slider video-slider">
					<div class="posts-slider" dir="rtl">
						<?php foreach ($fields['single_video_slider'] as $video) : ?>
							<div class="video-slide">
								<div class="about-video video-item" style="background-image: url('<?= getYoutubeThumb($video['single_video_link']); ?>')">
									<span class="play-button" data-video="<?= getYoutubeId($video['single_video_link']); ?>">
										<img src="<?= ICONS ?>play-button.png">
									</span>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['post_gallery']) : $gallery = makeGallery($fields['post_gallery']);?>
	<section class="gallery-block p-block" id="load-gallery">
		<div class="container">
			<div class="row">
				<div class="col-auto">
					<h2 class="block-title">גלריית תמונות מהפרוייקט: </h2>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="gallery-output">
						<?php foreach ($gallery as $gal_item) {
							get_template_part('views/partials/card', 'gallery', [
									'gallery' => $gal_item,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
		<?php if (count($gallery) > 1) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto mt-3">
						<span class="simple-link block-link more-gallery" id="load-more-items">
							טען עוד..
						</span>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/video', 'modal');
get_template_part('views/partials/repeat', 'form_gray');
$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = '';
switch ($currentType) {
	case 'project':
		$taxname = 'project_cat';
		$sameTitle = 'פרויקטים נוספים';
		break;
	case 'post':
		$taxname = 'category';
		$sameTitle = 'מאמרים נוספים';
		break;
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 5,
	'post_type' => $currentType,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $taxname,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 5,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
if ($fields['same_posts_title']) {
	$sameTitle = $fields['same_posts_title'];
}
if ($samePosts) : ?>
	<section class="posts-output arrows-slider p-block">
		<div class="container">
			<div class="row">
				<div class="col-auto mb-3">
					<h2 class="block-title"><?= $sameTitle; ?></h2>
				</div>
				<div class="col-12 post-col-base process-slider">
					<div class="posts-slider" dir="rtl">
						<?php foreach ($samePosts as $post) : ?>
							<div>
								<?php get_template_part('views/partials/card', 'post',
									[
										'post' => $post,
									]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
}
get_footer(); ?>
