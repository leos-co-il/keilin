<?php

get_header();
$query = get_queried_object();
$cat = $query->term_id;
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'project',
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $cat,
		]
	],
]);
$count_posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'project',
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $cat,
		]
	],
]);
$published_posts = count($count_posts);
?>

<article class="page-body p-block">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="posts-output">
		<div class="container">
			<div class="row justify-content-center mb-4">
				<div class="col-12 d-flex flex-column justify-content-center align-items-center">
					<h1 class="block-title text-center"><?= $query->name; ?></h1>
					<div class="base-output text-center">
						<?= category_description(); ?>
					</div>
				</div>
			</div>
			<?php if ($posts->have_posts()) : ?>
				<div class="row align-self-stretch vacas justify-content-center">
					<?php foreach ($posts->posts as $post) {
						get_template_part('views/partials/card', 'post_ajax',
							[
								'post' => $post,
							]);
					} ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($published_posts && $published_posts > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="simple-link block-link more-link" data-type="post">
						טען עוד...
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form_gray');
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => get_field('faq_title', $query),
			'faq' => $faq,
		]);
}
get_footer(); ?>
