<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$tel_2 = opt('tel_2');
$mail = opt('mail');
$mail_2 = opt('mail_2');
$address = opt('address');
$fax = opt('fax');
$map = opt('map_image');
?>

<article class="page-body">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="contact-page-body">
		<div class="container">
			<div class="row justify-content-center mb-4">
				<div class="col-12 d-flex flex-column justify-content-center align-items-center">
					<h1 class="block-title text-center"><?php the_title(); ?></h1>
					<div class="base-output">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="row justify-content-center">
						<?php if ($tel || $tel_2) : ?>
							<div class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
							   data-wow-delay="0.2s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<?php if ($tel) : ?>
									<h3 class="contact-info-title">
										התקשרו אלינו
									</h3>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										טלפון ראשי: <?= $tel; ?>
									</a>
								<?php endif;
								if ($tel_2) : ?>
									<h3 class="contact-info-title">
										אדריכל החברה אייל קיילין:
									</h3>
									<a href="tel:<?= $tel_2; ?>" class="contact-info">
										<?= $tel_2; ?>
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php if ($mail || $mail_2) : ?>
							<div class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
							   data-wow-delay="0.6s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<?php if ($mail) : ?>
									<h3 class="contact-info-title">
										שלחו לנו מייל
									</h3>
									<a href="mailto:<?= $mail; ?>" class="contact-info">
										<?= $mail; ?>
									</a>
								<?php endif;
								if ($mail_2) : ?>
									<h3 class="contact-info-title">
										פניות בנושא תוספת מרפסות
										תמא 38  \ התחדשות עירונית
									</h3>
									<a href="mailto:<?= $mail_2; ?>" class="contact-info">
										<?= $mail_2; ?>
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<?php if ($address) : ?>
							<a href="https://www.waze.com/ul?q=<?= $address; ?>"
							   class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</div>
								<h3 class="contact-info-title">
									מיקום:
								</h3>
								<p class="contact-info">
									<?= $address; ?>
								</p>
							</a>
						<?php endif; ?>
						<?php if ($fax) : ?>
							<div class="contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.4s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-fax.png">
								</div>
								<h3 class="contact-info-title">
									שלחו לנו פקס
								</h3>
								<div class="contact-info">
									<?= $fax; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<div class="post-form-wrap mb-5 wow zoomIn" data-wow-delay="0.4s">
						<div class="row justify-content-center pt-5 pb-5">
							<div class="col-xl-9 col-lg-10 col-11">
								<div class="row justify-content-center align-items-center mb-4">
									<?php if ($f_title = $fields['contact_form_title']) : ?>
										<div class="col-auto">
											<h2 class="form-title-big"><?= $f_title; ?></h2>
										</div>
									<?php endif; ?>
									<?php if ($f_text = $fields['contact_form_text']) : ?>
										<div class="col-auto">
											<p class="form-title-small">
												<?= $f_text; ?>
											</p>
										</div>
									<?php endif; ?>
								</div>
								<div class="contact-page-form">
									<?php getForm('59'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-10 col-12">
			<?php if ($map) : ?>
				<a class="map-image" href="<?= $map['url']; ?>" data-lightbox="map">
					<img src="<?= $map['url']; ?>" alt="map">
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
