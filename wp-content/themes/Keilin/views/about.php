<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body p-block">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center align-items-start">
			<div class="col-lg col-12 d-flex flex-column align-items-start justify-content-start">
				<h1 class="block-title"><?php the_title(); ?></h1>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-1 hide-this"></div>
			<?php if ($fields['about_video'] || has_post_thumbnail()) : ?>
				<div class="col-xl-4 col-lg-5 col-12 graph-about-col">
					<div class="about-video" <?php if(has_post_thumbnail()): ?>
							style="background-image: url('<?= postThumb(); ?>')"<?php endif; ?>>
						<?php if ($fields['about_video']) : ?>
						<span class="play-button" data-video="<?= getYoutubeId($fields['about_video']); ?>">
							<img src="<?= ICONS ?>play-button.png">
						</span>
						<?php endif; ?>
						<div class="about-date-wrap">
							<span class="tel-block-wrap tel-top mb-3 font-weight-normal">קיימים משנת</span>
							<h3 class="year-main">1981</h3>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/video', 'modal');
get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'form_gray');
if ($fields['ab_service_item']) {
	get_template_part('views/partials/content', 'services', [
			'title' => $fields['ab_serv_block_title'],
			'services' => $fields['ab_service_item'],
	]);
}
get_template_part('views/partials/repeat', 'partners');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
