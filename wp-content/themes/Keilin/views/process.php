<?php
/*
Template Name: תהליך בנייה
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body p-block">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container mb-5">
		<div class="row">
			<div class="col-12">
				<h1 class="base-title text-center"><?php the_title(); ?></h1>
				<div class="base-output text-center"><?php the_content(); ?></div>
			</div>
		</div>
	</div>
	<?php if ($fields['process_step']) : $all = count($fields['process_step']); ?>
		<div class="container arrows-slider process-slider">
			<div class="row">
				<div class="col-12">
					<div class="base-slider" dir="rtl">
						<?php foreach ($fields['process_step'] as $n => $step) : ?>
							<div class="slide-pad">
								<div class="row justify-content-center align-items-center process-order">
									<div class="col-lg col-12 d-flex flex-column align-items-start justify-content-start">
										<h2 class="block-title"><?= 'שלב '.($n + 1).' | '.$all; ?></h2>
										<h3 class="step-title"><?= $step['step_title']; ?></h3>
										<div class="base-output">
											<?= $step['step_text']; ?>
										</div>
									</div>
									<div class="col-1 hide-this"></div>
									<div class="col-xl-4 col-lg-5 col-12 graph-about-col">
										<div class="about-video" <?php if($step['step_img']): ?>
											style="background-image: url('<?= $step['step_img']['url']; ?>')"<?php endif; ?>>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'form_gray');
if ($fields['process_gallery']) : $gallery = makeGallery($fields['process_gallery']);?>
	<section class="gallery-block p-block" id="load-gallery">
		<div class="container">
			<?php if ($fields['process_gallery_title']) : ?>
				<div class="row">
					<div class="col-auto">
						<h2 class="block-title"><?= $fields['process_gallery_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-12">
					<div class="gallery-output">
						<?php foreach ($gallery as $gal_item) {
							get_template_part('views/partials/card', 'gallery', [
									'gallery' => $gal_item,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
		<?php if (count($gallery) > 1) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto mt-3">
						<span class="simple-link block-link more-gallery" id="load-more-items">
							טען עוד..
						</span>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
}
get_footer(); ?>
