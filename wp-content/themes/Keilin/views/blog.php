<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['choose_type'] ? $fields['choose_type'] : 'post';
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => $type,
]);
$published_posts = '';
$count_posts = wp_count_posts();
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
?>

<article class="page-body p-block">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="posts-output">
		<div class="container">
			<div class="row justify-content-center mb-4">
				<div class="col-12 d-flex flex-column justify-content-center align-items-center">
					<h1 class="block-title text-center"><?php the_title(); ?></h1>
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php if ($posts->have_posts()) : ?>
				<div class="row align-self-stretch vacas justify-content-center">
					<?php foreach ($posts->posts as $post) {
						get_template_part('views/partials/card', 'post_ajax',
								[
										'post' => $post,
								]);
					} ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($published_posts && $published_posts > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="simple-link block-link more-link" data-type="post">
						טען עוד...
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form_gray');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
