<?php if (isset($args['content']) && $args['content']) :
$sliderImage = isset($args['img']) ? $args['img'] : NULL; ?>
<section class="slider-base-wrap p-block arrows-slider">
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<?php if ($sliderImage) : ?>
				<div class="col-lg-5 d-flex justify-content-center align-items-center">
					<img src="<?= $sliderImage['url']; ?>" class="slider-img">
				</div>
			<?php endif; ?>
			<div class="col-lg-7 col-12 slider-wrap-col">
				<div class="slider-text-wrap">
					<div class="base-slider" dir="rtl">
						<?php foreach ($args['content'] as $content) : ?>
							<div>
								<div class="base-output"><?= $content['content']; ?></div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
