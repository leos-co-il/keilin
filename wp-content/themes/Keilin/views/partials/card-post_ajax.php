<?php if (isset($args['post'])) : $post = $args['post']; ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col post-col-base">
		<div class="post-item post-item-bordered more-card" data-id="<?= $post->ID; ?>">
			<div class="post-item-image"
					<?php if (has_post_thumbnail($post)) : ?>
						style="background-image: url('<?= postThumb($post); ?>')"
					<?php endif;?>>
			</div>
			<div class="post-item-content">
				<h2 class="post-item-title"><?= $post->post_title; ?></h2>
				<p class="base-text mb-3">
					<?= text_preview($post->post_content, 10); ?>
				</p>
			</div>
			<a href="<?php the_permalink($post); ?>" class="simple-link align-self-end">
				קרא עוד
			</a>
		</div>
	</div>
<?php endif; ?>
