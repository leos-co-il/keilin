<?php if (isset($args['services']) && $args['services']) : ?>
	<section class="home-services">
		<?php if (isset($args['title']) && $args['title']) : ?>
			<div class="container">
				<div class="row">
					<div class="col-auto">
						<h2 class="block-title"><?= $args['title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="green-line">
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($args['services'] as $x => $serv_item) : ?>
						<div class="col-md-4 col-sm-10 col-12 mb-3 service-col">
							<div class="serv-link-item service-item wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
								<div class="s-icon-wrap">
									<?php if ($serv_item['h_serv_icon']) : ?>
										<img src="<?= $serv_item['h_serv_icon']['url']; ?>" alt="service-icon">
									<?php endif; ?>
								</div>
								<div class="flex-grow-1 serv-content-wrap">
									<h4 class="mid-text"><?= $serv_item['h_serv_title']; ?></h4>
									<p class="about-link-text mb-3"><?= $serv_item['h_serv_text']; ?></p>
								</div>
								<a href="<?= $serv_item['h_service_link']; ?>" class="simple-link">
									קרא עוד
								</a>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
