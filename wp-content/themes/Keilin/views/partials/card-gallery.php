<?php if (isset($args['gallery']) && $args['gallery']) : ?>
	<div class="gallery-part">
		<?php foreach ($args['gallery'] as $x => $col) : ?>
			<div class="gallery-col">
				<?php foreach ($col as $img) : ?>
					<a class="gallery-item" style="background-image: url('<?= $img['url']; ?>')"
					href="<?= $img['url']; ?>" data-lightbox="gallery">
						<span class="gallery-overlay">+</span>
					</a>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
