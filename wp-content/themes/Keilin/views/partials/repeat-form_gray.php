<section class="repeat-form-block gray-form">
	<div class="container pt-4">
		<div class="row align-items-stretch justify-content-center">
			<div class="col-xl-4 col-12 d-flex flex-column justify-content-center col-border-form">
				<?php if ($rep_title = opt('rep_form_title')) : ?>
					<h2 class="form-title-big text-right"><?= $rep_title; ?></h2>
				<?php endif; ?>
				<?php if ($rep_text = opt('rep_form_text')) : ?>
					<p class="form-title-small text-right">
						<?= $rep_text; ?>
					</p>
				<?php endif; ?>
			</div>
			<div class="col-xl-8 col-12 middle-form wow zoomIn d-flex align-items-center justify-content-center">
				<?php getForm('58'); ?>
			</div>
		</div>
	</div>
</section>
