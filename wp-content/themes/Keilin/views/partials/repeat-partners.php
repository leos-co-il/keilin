<?php
$text = opt('partners_text');
$partners = opt('partners_logo');
$title = opt('partners_title');
if ($text || $partners) : ?>
	<div class="partners-block p-block">
		<div class="container">
			<div class="row justify-content-center align-items-start mb-5">
				<div class="col-lg-4 col-12 d-flex justify-content-start">
					<h2 class="block-title">
						<?= $title ? $title : 'שיתופי פעולה'; ?>
					</h2>
				</div>
				<?php if ($text) : ?>
					<div class="col-lg-8 col-12">
						<div class="base-output">
							<?= $text; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($partners) : ?>
				<div class="row justify-content-center align-self-stretch">
					<?php foreach ($partners as $n => $partner_item) : ?>
						<div class="col-xl-3 col-lg-4 col-6 partner-item wow fadeIn" data-wow-delay="0.<?= $n + 2; ?>s">
							<img src="<?= $partner_item['url']; ?>">
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;
			if ($link = opt('partners_link')) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mt-3">
						<a class="simple-link block-link" href="<?= $link['url']; ?>">
							<?= (isset($link['title']) && $link) ? $link['title'] : 'הצג עוד לקוחות'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
