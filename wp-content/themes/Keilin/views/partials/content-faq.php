<?php if (isset($args['faq'])) : ?>
	<section class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-10 col-sm-11 col-12 d-flex">
					<h2 class="block-title mb-5">
						<?= (isset($args['block_title']) && ['block_title']) ? $args['block_title'] : 'שאלות נפוצות'; ?>
					</h2>
				</div>
				<div class="col-xl-9 col-lg-10 col-sm-11 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="btn question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<?= $item['faq_question']; ?>
										<img src="<?= ICONS ?>faq-arrow-down.png" class="arrow-top" alt="arrow-top">
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="answer-body base-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
