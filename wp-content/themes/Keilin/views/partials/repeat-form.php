<section class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-11 col-md-10 col-sm-11 col-12 form-borders">
				<img src="<?= IMG ?>border-top.png" class="bt-img">
				<img src="<?= IMG ?>border-bottom.png" class="bb-img">
				<div class="row align-items-stretch justify-content-center mb-3">
					<?php if ($rep_title = opt('rep_form_title')) : ?>
						<div class="col-auto form-col">
							<h2 class="form-title-big"><?= $rep_title; ?></h2>
						</div>
					<?php endif; ?>
					<?php if ($rep_text = opt('rep_form_text')) : ?>
						<div class="col-auto form-col">
							<p class="form-title-small">
								<?= $rep_text; ?>
							</p>
						</div>
					<?php endif; ?>
				</div>
				<div class="middle-form wow zoomIn">
					<?php getForm('58'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
