<?php
$benefits = opt('benefits');
$counter = opt('counter');
if ($benefits || $counter) : ?>
	<div class="gen-benefits-block">
		<div class="container">
			<div class="row justify-content-between">
				<?php if ($counter) : ?>
					<div class="col-lg-4 col-12 counter-block" id="start-count">
						<div class="counter-wrap-custom">
							<?php foreach ($counter as $count) : ?>
								<div class="counter-item wow fadeIn">
									<h2 class="counter-number counter-num" data-from="1"
										data-to="<?= $count['counter_num']; ?>" data-speed="1500">
										<?= $count['counter_num']; ?>
									</h2>
									<h4 class="counter-title"><?= $count['counter_text']; ?></h4>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif;
				if ($benefits) : ?>
					<div class="col-lg-8 col-12">
						<div class="benefits-col">
							<div class="row justify-content-center align-items-start">
								<?php foreach ($benefits as $ben_item) : ?>
									<div class="col-xl-4 col-md-6 my-3">
										<div class="benefit-icon">
											<?php if ($ben_item['ben_icon']) : ?>
												<img src="<?= $ben_item['ben_icon']['url']; ?>">
											<?php endif; ?>
										</div>
										<h3 class="benefit-title"><?= $ben_item['ben_title']; ?></h3>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif;
