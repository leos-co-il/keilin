<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>
<section class="home-main"
	<?php if ($fields['home_back_img']) : ?>
		style="background-image: url('<?= $fields['home_back_img']['url']; ?>')"
	<?php endif; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-8 col-sm-10 col-12 d-flex flex-column justify-content-start align-items-start home-col-content">
				<img src="<?= IMG ?>border-main.png" class="border-main">
				<?php if ($fields['home_main_title']) : ?>
					<h2 class="home-main-title"><?= $fields['home_main_title']; ?></h2>
				<?php endif; ?>
				<div class="d-flex align-items-center">
					<?php if ($logo = opt('logo')) : ?>
						<a href="/" class="logo home-small-logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					<?php endif;
					if ($fields['home_main_text']) : ?>
						<p class="mid-text"><?= $fields['home_main_text']; ?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="home-about">
	<?php if ($fields['home_main_servs']) : ?>
		<div class="home-serv-links">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="row">
							<?php foreach ($fields['home_main_servs'] as $num => $serv_l) : ?>
								<div class="col-6 col-serv">
									<div class="serv-link-item wow fadeInRight" data-wow-delay="0.<?= $num; ?>s">
										<span class="serv-num">
											<?= '0'.($num + 1); ?>
										</span>
										<div>
											<h4 class="mid-text"><?= $serv_l['main_step_title']; ?></h4>
											<a href="<?= $serv_l['main_step_link']; ?>" class="simple-link">
												קרא עוד
											</a>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>

		</div>
	<?php endif; ?>
	<div class="main-home-form">
		<div class="container">
			<div class="row">
				<?php if ($fields['main_form_title'] || $fields['main_form_text']) : ?>
					<div class="col-lg-4 d-flex flex-column justify-content-start align-items-start form-home-text">
						<h2 class="form-title-big"><?= $fields['main_form_title']; ?></h2>
						<p class="form-title-small"><?= $fields['main_form_text']; ?></p>
					</div>
				<?php endif; ?>
				<div class="col">
					<?php getForm('58'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="home-about-content">
		<div class="container">
			<div class="row justify-content-center mb-4">
				<?php if ($fields['home_about_title']) : ?>
					<div class="col-lg-4 col-12 d-flex justify-content-start align-items-start">
						<h2 class="block-title"><?= $fields['home_about_title']; ?></h2>
					</div>
				<?php endif;
				if ($fields['home_about_text']) : ?>
					<div class="col-lg-8 col-12">
						<div class="base-output"><?= $fields['home_about_text']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<div class="row justify-content-center">
				<?php if ($fields['home_about_title']) : ?>
					<div class="col-lg-4 col-12 d-flex flex-column justify-content-center align-items-start wow zoomIn">
						<span class="tel-block-wrap tel-top mb-3">קיימים משנת</span>
						<h3 class="year-main">1981</h3>
					</div>
				<?php endif;
				if ($fields['home_about_links']) : ?>
					<div class="col-lg-8 col-12">
						<div class="row align-items-stretch justify-content-start">
							<?php foreach ($fields['home_about_links'] as $num => $about_serv_l) : ?>
								<div class="col-xl-4 col-6 col-serv col-serv-text">
									<div class="serv-link-item flex-column justify-content-between h-100">
										<div class="flex-grow-1">
											<h4 class="mid-text"><?= $about_serv_l['h_a_link_title']; ?></h4>
											<p class="about-link-text mb-3"><?= $about_serv_l['h_a_link_text']; ?></p>
										</div>
										<a href="<?= $about_serv_l['h_a_link']; ?>" class="simple-link">
											קרא עוד
										</a>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_template_part('views/partials/repeat', 'benefits');
if ($fields['h_service_item']) {
	get_template_part('views/partials/content', 'services', [
			'title' => $fields['h_serv_block_title'],
			'services' => $fields['h_service_item'],
	]);
}
get_template_part('views/partials/repeat', 'partners');
if ($fields['home_project']) : ?>
	<section class="project-slider-block arrows-slider">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6 col-12">
					<div class="project-slider" dir="rtl">
						<?php foreach ($fields['home_project'] as $content) : ?>
							<div>
								<div class="projects-img-col">
									<div class="project-sl-img" <?php if ($content['pr_img']) : ?>
										style="background-image: url('<?= $content['pr_img']['url']; ?>')"
									<?php endif; ?>>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-lg-6 col-12">
					<?php if ($fields['home_projects_title']) : ?>
						<h2 class="block-title mb-4"><?= $fields['home_projects_title']; ?></h2>
					<?php endif; ?>
					<div class="project-slider-nav" dir="rtl">
						<?php foreach ($fields['home_project'] as $content) : ?>
							<div>
								<div class="d-flex flex-column justify-content-center align-items-start">
									<?php if ($content['pr_title']) : ?>
										<div class="d-flex justify-content-start align-items-center mb-3">
											<?php if ($logo = opt('logo')) : ?>
												<a href="/" class="logo home-small-logo">
													<img src="<?= $logo['url'] ?>" alt="logo">
												</a>
											<?php endif; ?>
											<p class="mid-text"><?= $content['pr_title']; ?></p>
										</div>
									<?php endif; ?>
									<div class="base-output"><?= $content['pr_text']; ?></div>
									<?php if ($content['pr_link']) : ?>
										<a class="simple-link block-link align-self-end" href="<?= $content['pr_link']['url']; ?>">
											<?= (isset($content['pr_link']['title']) && $content['pr_link']['title']) ? $content['pr_link']['title'] : 'לצפייה בתוכנית בנייה'; ?>
										</a>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_work_text'] || $fields['h_work_link']) : ?>
	<section class="info-block">
	<div class="info-block-text">
		<h2 class="block-title">
			<?= $fields['h_work_title'] ? $fields['h_work_title'] : 'איך זה עובד?'; ?>
		</h2>
		<?php if ($fields['h_work_text']) : ?>
			<div class="base-output info-output">
				<?= $fields['h_work_text']; ?>
			</div>
		<?php endif;
		if ($fields['h_work_link']) : ?>
			<a class="simple-link block-link info-link-white" href="<?= $fields['h_work_link']['url']; ?>">
				<?= (isset($fields['h_work_link']['title']) && $fields['h_work_link']['title']) ? $fields['h_work_link']['title'] : 'לצפייה בתוכנית בנייה'; ?>
			</a>
		<?php endif; ?>
	</div>
		<?php if ($fields['h_work_img']) : ?>
			<div class="info-block-img">
				<img src="<?= $fields['h_work_img']['url']; ?>">
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['review_item']) : ?>
	<!--Reviews-->
	<section class="reviews p-block">
		<div class="container">
			<?php if ($fields['reviews_block_title']) : ?>
				<div class="row justify-content-start">
					<div class="col-auto">
						<div class="block-title"><?= $fields['reviews_block_title']; ?></div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center arrows-slider reviews-arrows">
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($fields['review_item'] as $number => $revItem) : ?>
							<div class="review-slide">
								<div class="review-item">
									<div class="rev-pop-trigger">
										+
										<div class="hidden-review">
											<h3 class="review-name"><?= $revItem['rev_name']; ?></h3>
											<div class="base-output">
												<?= $revItem['rev_text']; ?>
											</div>
										</div>
									</div>
									<?php if ($rev_img = $revItem['rev_img']) : ?>
										<div class="review-logo">
											<img src="<?= $rev_img['url']; ?>">
										</div>
									<?php endif; ?>
									<div class="rev-content">
										<h3 class="review-name"><?= $revItem['rev_name']; ?></h3>
										<div class="base-text review-prev">
											<?= text_preview($revItem['rev_text'], '20'); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Reviews pop-up-->
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['home_gallery']) : $gallery = makeGallery($fields['home_gallery']);?>
	<section class="gallery-block p-block home-gallery">
		<?php if ($fields['h_gallery_title']) : ?>
			<div class="container">
				<div class="row">
					<div class="col-auto">
						<h2 class="block-title"><?= $fields['h_gallery_title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="gallery-output">
			<?php foreach ($gallery as $gal_item) {
				get_template_part('views/partials/card', 'gallery', [
						'gallery' => $gal_item,
				]);
			} ?>
		</div>
		<?php if ($fields['h_gallery_link']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto mt-3">
						<a class="simple-link block-link" href="<?= $fields['h_gallery_link']['url']; ?>">
							<?= (isset($fields['h_gallery_link']['title']) && $fields['h_gallery_link']['title']) ? $fields['h_gallery_link']['title'] : 'לכל הגלריה'; ?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($slider_seo = $fields['single_slider_seo']) {
get_template_part('views/partials/content', 'slider', [
		'content' => $slider_seo,
		'img' => $fields['slider_img'],
]);
}
if ($fields['home_posts']) : ?>
	<section class="posts-output p-block">
		<?php if ($fields['h_posts_title']) : ?>
			<div class="container">
				<div class="row justify-content-start">
					<div class="col-auto">
						<h2 class="block-title mb-4"><?= $fields['h_posts_title']; ?></h2>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch home-post-row">
				<?php foreach ($fields['home_posts'] as $post) : ?>
					<div class="col-xl-3 col-sm-6 col-12 mb-4 h-post-col post-col">
						<?php get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mt-3">
						<a class="simple-link block-link" href="<?= $fields['h_posts_link']['url']; ?>">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ? $fields['h_posts_link']['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
}
 get_footer(); ?>
