<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();
?>

<article class="article-page-body page-body p-block">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col–auto">
				<h1 class="block-title"><?php the_title(); ?></h1>
			</div>
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['main_tab']) : ?>
		<div class="gallery-body">
			<div class="container main-tabs-line">
				<ul class="nav nav-tabs row justify-content-center" id="maingalTab" role="tablist">
					<?php foreach ($fields['main_tab'] as $main => $tab) : ?>
						<li class="nav-item col-auto">
							<a class="nav-link <?= ($main === 0) ? 'active' : ''; ?>" id="maingal-<?= $main; ?>-tab" data-toggle="tab" href="#maingal-<?= $main; ?>" role="tab"
							   aria-controls="maingal-<?= $main; ?>" aria-selected="<?= ($main === 0) ? 'true' : 'false'; ?>">
								<?= $tab['main_tab_title']; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="tab-content gallery-content-tab" id="maingal-tabContent">
				<?php foreach ($fields['main_tab'] as $main => $tab) : ?>
					<div class="tab-pane fade <?= ($main === 0) ? 'show active' : ''; ?>" id="maingal-<?= $main; ?>"
						 role="tabpanel" aria-labelledby="maingal-<?= $main; ?>-tab">
						<?php if ($tab['subgalleries']) : ?>
							<div class="subgalleries-body">
								<nav class="container">
									<ul class="nav nav-tabs row justify-content-center align-items-stretch"
										id="nav-tab" role="tablist">
										<?php foreach ($tab['subgalleries'] as $x => $sub_tab) : ?>
											<li class="nav-item col-lg-3 col-md-6 col-12 mb-3" role="presentation">
												<a class="nav-item nav-link <?= ($x === 0) ? 'active' : ''; ?>" id="nav-<?= $x; ?>-tab" data-toggle="tab" href="#nav-<?= $x; ?>" role="tab"
												   aria-controls="nav-<?= $x; ?>" aria-selected="<?= ($x === 0) ? 'true' : 'false'; ?>">
													<?= $sub_tab['subgallery_title']; ?>
												</a>
											</li>
										<?php endforeach; ?>
									</ul>
								</nav>
								<div class="tab-content">
									<?php foreach ($tab['subgalleries'] as $x => $sub_tab) : ?>
										<div class="tab-pane fade <?= ($x === 0) ? 'show active' : ''; ?>" id="nav-<?= $x; ?>"
											 role="tabpanel" aria-labelledby="nav-<?= $x; ?>-tab">
											<?php $gallery_chunked = makeGallery($sub_tab['subgallery_item']); ?>
											<div class="gallery-output load-gallery">
												<?php foreach ($gallery_chunked as $gal_item) :
													get_template_part('views/partials/card', 'gallery', [
															'gallery' => $gal_item,
													]);
												endforeach; ?>
												<?php if (count($gallery_chunked) > 1) : ?>
													<span class="simple-link block-link more-gallery take-gals">
													טען עוד...
												</span>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form_gray');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
}
get_footer(); ?>



