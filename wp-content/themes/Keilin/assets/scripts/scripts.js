(function($) {
	//Load more items
	var sizeLi = $('#load-gallery .gallery-part').size();
	var x = 1;
	$('#load-gallery .gallery-part:lt('+x+')').addClass('show');
	$('#load-more-items').click(function () {
		x = (x + 1 <= sizeLi) ? x + 1 : sizeLi;
		$('#load-gallery .gallery-part:lt('+x+')').addClass('show');
		if (x === sizeLi) {
			$('#load-more-items').addClass('hide');
		}
	});

	$('.take-gals').click(function () {
		var parentGal = $(this).parent('.load-gallery');
		var sizeL = parentGal.children('.gallery-part').size();
		console.log(sizeL);
		var y = 1;
		y = (y + 1 <= sizeL) ? y + 1 : sizeL;
		parentGal.children('.gallery-part:lt('+x+')').addClass('show');
		if (y === sizeL) {
			$(this).addClass('hide');
		}
	});
	$( document ).ready(function() {
		$('.counter-number').rCounter({
			duration: 30
		});
		$('.project-slider').slick({
			rtl: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.project-slider-nav'
		});
		$('.project-slider-nav').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.project-slider',
			dots: false,
			arrows: true,
			centerMode: false,
			focusOnSelect: true
		});
		$('.posts-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.toggleClass( 'show-menu' );
		});
		$('.pop-trigger').click(function () {
			$('.float-form').addClass('show-form');
			$('.pop-body').addClass('body-hidden');
		});
		$('.close-pop').click(function () {
			$('.float-form').removeClass('show-form');
			$('.pop-body').removeClass('body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		});
		$('.reviews-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
	});

	//More posts
	$('.more-link').click(function() {
		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});

		var type = $(this).data('type');
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: 'json',
			data: {
				type: type,
				ids: ids,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.more-link').addClass('hide-more');
				}
				$('.vacas').append(data.html);
			}
		});
	});
})( jQuery );
